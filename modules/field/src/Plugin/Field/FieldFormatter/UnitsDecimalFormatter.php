<?php

namespace Drupal\units_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\DecimalFormatter;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'units_decimal' field formatter.
 *
 * @FieldFormatter(
 *   id = "units_decimal",
 *   label = @Translation("Number field with units"),
 *   field_types = {
 *     "units_decimal"
 *   }
 * )
 */
class UnitsDecimalFormatter extends DecimalFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();
    $settings['unit_format'] = '';
    $settings['unit_convert'] = '';

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $entity_type_manager = \Drupal::service('entity_type.manager');

    $elements['unit_format'] = [
      '#title' => t('Unit format'),
      '#type' => 'select',
      '#options' => [
        '' => $this->t('- None -'),
      ],
      '#default_value' => $this->getSetting('unit_format'),
    ];

    $format_storage = $entity_type_manager->getStorage('units_format');
    foreach ($format_storage->loadMultiple() as $format) {
      $elements['unit_format']['#options'][$format->id()] = $format->label();
    }

    $settings = $this->getFieldSettings();
    $measure_storage = $entity_type_manager->getStorage('units_measure');
    $measure = $measure_storage->load($settings['measure']);
    $options = [
      '' => $this->t('- None -'),
    ];

    $all_values = $settings['units'];
    $allowed_values = array_intersect(array_keys($all_values), $all_values);

    foreach ($measure->getUnits() as $unit) {
      if (empty($allowed_values) || in_array($unit->id(), $allowed_values)) {
        $options[$unit->id()] = $unit->label();
      }
    }

    $elements['unit_convert'] = [
      '#title' => t('Convert to unit'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $this->getSetting('unit_convert'),
    ];

    unset($elements['prefix_suffix']);

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->numberFormat(1234.1234567890);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $settings = $this->getFieldSettings();

    $entity_type_manager = \Drupal::service('entity_type.manager');
    $unit_storage = $entity_type_manager->getStorage('units_unit');

    foreach ($items as $delta => $item) {
      if ($to = $this->getSetting('unit_convert')) {
        $value = units_convert($item->value, $item->unit, $to);
        $output = $this->numberFormat($value);
        $target = $to;
      }
      else {
        $value = $item->value;
        $output = $this->numberFormat($value);
        $target = $item->unit;
      }

      $format_storage = $entity_type_manager->getStorage('units_format');
      $format = $format_storage->load($this->getSetting('unit_format'));
      $unit = $unit_storage->load($target);

      if (isset($format) && isset($unit)) {
        foreach ($unit->getFormatters() as $formatter) {
          if ($formatter->getFormatId() === $format->id()) {
            $output = $formatter->format($value, $unit, [
              'scale' => $this->getSetting('scale'),
              'decimal_separator' => $this->getSetting('decimal_separator'),
              'thousand_separator' => $this->getSetting('thousand_separator'),
            ]);
            break;
          }
        }
      }

      // Output the raw value in a content attribute if the text of the HTML
      // element differs from the raw value (for example when a prefix is used).
      if (isset($item->_attributes) && $item->value != $output) {
        $item->_attributes += ['content' => $item->value];
      }

      $elements[$delta] = ['#markup' => $output];
    }

    return $elements;
  }

}

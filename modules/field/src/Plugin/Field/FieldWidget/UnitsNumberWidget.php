<?php

namespace Drupal\units_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\NumberWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'units_number' field widget.
 *
 * @FieldWidget(
 *   id = "units_number",
 *   label = @Translation("Number field with units"),
 *   field_types = {
 *     "units_decimal"
 *   }
 * )
 */
class UnitsNumberWidget extends NumberWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : NULL;
    $settings = $this->getFieldSettings();

    $element['#type'] = 'fieldgroup';
    $element['#attributes'] = [
      'class' => ['fieldgroup', 'form-composite', 'container-inline'],
    ];

    $element['value'] = [
      '#type' => 'number',
      '#default_value' => $value,
      '#placeholder' => $this->getSetting('placeholder'),
    ];

    // Set the step for floating point and decimal numbers.
    switch ($this->fieldDefinition->getType()) {
      case 'units_decimal':
        $element['value']['#step'] = pow(0.1, $settings['scale']);
        break;

      case 'units_float':
        $element['value']['#step'] = 'any';
        break;
    }

    // Set minimum and maximum.
    if (is_numeric($settings['min'])) {
      $element['value']['#min'] = $settings['min'];
    }
    if (is_numeric($settings['max'])) {
      $element['value']['#max'] = $settings['max'];
    }

    $entity_type_manager = \Drupal::service('entity_type.manager');
    $measure_storage = $entity_type_manager->getStorage('units_measure');
    $measure = $measure_storage->load($settings['measure']);
    $options = [];

    $all_values = $settings['units'];
    $allowed_values = array_intersect(array_keys($all_values), $all_values);

    foreach ($measure->getUnits() as $unit) {
      if (empty($allowed_values) || in_array($unit->id(), $allowed_values)) {
        $options[$unit->id()] = $unit->label();
      }
    }

    $element['unit'] = [
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => isset($items[$delta]->unit) ? $items[$delta]->unit : NULL,
    ];

    return $element;
  }

}

<?php

namespace Drupal\units_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\DecimalItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'units_decimal' field type.
 *
 * @FieldType(
 *   id = "units_decimal",
 *   label = @Translation("Number (decimal, with units)"),
 *   description = @Translation("This field stores a number in the database in a fixed decimal format with units."),
 *   category = @Translation("Number"),
 *   default_widget = "units_number",
 *   default_formatter = "units_decimal"
 * )
 */
class UnitsDecimalItem extends DecimalItem {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['unit'] = DataDefinition::create('string')
      ->setLabel(t('Unit'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['unit'] = [
      'type' => 'varchar',
      'length' => 255,
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    $settings = parent::defaultStorageSettings();
    $settings['measure'] = '';

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);
    $settings = $this->getSettings();

    $entity_type_manager = \Drupal::service('entity_type.manager');
    $measure_storage = $entity_type_manager->getStorage('units_measure');
    $options = [];

    foreach ($measure_storage->loadMultiple() as $measure) {
      $options[$measure->id()] = $measure->label();
    }

    $element['measure'] = [
      '#title' => t('Measure'),
      '#type' => 'select',
      '#options' => $options,
      '#disabled' => $has_data,
      '#default_value' => $settings['measure'],
      '#description' => t('The type of unit to store.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $settings = parent::defaultFieldSettings();
    $settings['units'] = [];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);
    $settings = $this->getSettings();

    $entity_type_manager = \Drupal::service('entity_type.manager');
    $measure_storage = $entity_type_manager->getStorage('units_measure');
    $measure = $measure_storage->load($settings['measure']);
    $options = [];

    foreach ($measure->getUnits() as $unit) {
      $options[$unit->id()] = $unit->label();
    }

    $element['units'] = [
      '#type' => 'checkboxes',
      '#title' => t('Units'),
      '#options' => $options,
      '#default_value' => $settings['units'],
      '#description' => t('The units to allow. Leave blank to allow all.'),
    ];

    unset($element['prefix']);
    unset($element['suffix']);

    return $element;
  }

}

<?php

namespace Drupal\units_ui\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\units\MeasureInterface;
use Drupal\units\UnitInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the units measure unit overview form.
 */
class MeasureUnitsOverviewForm extends FormBase {

  /**
   * The measure entity.
   *
   * @var \Drupal\units\MeasureInterface
   */
  protected $measure;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new MeasureUnitsOverviewForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'units_measure_units_overview_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\units\MeasureInterface $units_measure
   *   The measure entity.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, MeasureInterface $units_measure = NULL) {
    $this->measure = $units_measure;

    $form['units'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#empty' => $this->t('There are no units yet.'),
    ];

    foreach ($this->measure->getUnits() as $unit) {
      $form['units'][$unit->id()] = $this->buildRow($unit);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Builds the header row for the unit listing.
   *
   * @return array
   *   A render array structure of header strings.
   */
  public function buildHeader() {
    $row['name'] = $this->t('Name');
    $row['operations'] = $this->t('Operations');
    return $row;
  }

  /**
   * Builds a row for a single entry in the units listing.
   *
   * @param \Drupal\units\UnitInterface $unit
   *   The unit for this row of the list.
   *
   * @return array
   *   A render array structure of row information.
   */
  public function buildRow(UnitInterface $unit) {
    $row['name']['#markup'] = $unit->label();
    $row['operations'] = $this->buildOperations($unit);
    return $row;
  }

  /**
   * Builds a renderable list of operation links for the unit.
   *
   * @param \Drupal\units\UnitInterface $unit
   *   The unit on which the linked operations will be performed.
   *
   * @return array
   *   A renderable array of operation links.
   */
  public function buildOperations(UnitInterface $unit) {
    return [
      '#type' => 'operations',
      '#links' => $this->getOperations($unit),
    ];
  }

  /**
   * Provides an array of information to build a list of operation links.
   *
   * @param \Drupal\units\UnitInterface $unit
   *   The unit the operations are for.
   *
   * @return array
   *   An associative array of operation link data for this list.
   */
  public function getOperations(UnitInterface $unit) {
    $operations['remove'] = [
      'title' => $this->t('Remove'),
      'weight' => -10,
      'url' => Url::fromRoute('units_ui.measure_units_remove_form', [
        'units_measure' => $this->measure->id(),
        'units_unit' => $unit->id(),
      ]),
    ];

    $operations += $this->entityTypeManager->getListBuilder('units_unit')->getOperations($unit);

    return $operations;
  }

}

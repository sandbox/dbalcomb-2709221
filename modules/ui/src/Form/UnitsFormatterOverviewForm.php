<?php

namespace Drupal\units_ui\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\units\UnitInterface;
use Drupal\units\UnitsFormatterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the units formatter overview form.
 */
class UnitsFormatterOverviewForm extends FormBase {

  /**
   * The unit entity.
   *
   * @var \Drupal\units\UnitInterface
   */
  protected $unit;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new UnitsFormatterOverviewForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'units_formatter_overview_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\units\UnitInterface $units_unit
   *   The unit entity.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, UnitInterface $units_unit = NULL) {
    $this->unit = $units_unit;

    $form['formatters'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#empty' => $this->t('There are no formatters yet.'),
    ];

    foreach ($this->unit->getFormatters() as $formatter) {
      $form['formatters'][$formatter->getUuid()] = $this->buildRow($formatter);
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $formats = [];
    foreach ($form_state->getValue('formatters', []) as $uuid => $formatter) {
      if (!empty($formatter['format'])) {
        if (isset($formats[$formatter['format']])) {
          $form_state->setError($form['formatters'][$uuid]['format'], $this->t('A format can only be assigned to a single formatter.'));
        }
        $formats[$formatter['format']] = TRUE;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($this->unit->getFormatters() as $formatter) {
      $format = $form_state->getValue([
        'formatters',
        $formatter->getUuid(),
        'format',
      ], '');
      $config = $formatter->getConfiguration();
      $config['format'] = $format;
      $formatter->setConfiguration($config);
    }
    $this->unit->save();
  }

  /**
   * Builds the header row for the formatter listing.
   *
   * @return array
   *   A render array structure of header strings.
   */
  public function buildHeader() {
    $row['name'] = $this->t('Name');
    $row['summary'] = $this->t('Summary');
    $row['format'] = $this->t('Format');
    $row['operations'] = $this->t('Operations');
    return $row;
  }

  /**
   * Builds a row for a single entry in the formatter listing.
   *
   * @param \Drupal\units\UnitsFormatterInterface $formatter
   *   The formatter for this row of the list.
   *
   * @return array
   *   A render array structure of row information.
   */
  public function buildRow(UnitsFormatterInterface $formatter) {
    $row['name']['#markup'] = $formatter->getLabel();
    $row['summary'] = $formatter->getSummary($this->unit);

    $row['format'] = [
      '#type' => 'select',
      '#empty_option' => '- None -',
      '#empty_value' => '',
      '#default_value' => $formatter->getFormatId(),
    ];

    $format_storage = $this->entityTypeManager->getStorage('units_format');

    foreach ($format_storage->loadMultiple() as $format) {
      $row['format']['#options'][$format->id()] = $format->label();
    }

    $row['operations'] = $this->buildOperations($formatter);

    return $row;
  }

  /**
   * Builds a renderable list of operation links for the formatter.
   *
   * @param \Drupal\units\UnitsFormatterInterface $formatter
   *   The formatter on which the linked operations will be performed.
   *
   * @return array
   *   A renderable array of operation links.
   */
  public function buildOperations(UnitsFormatterInterface $formatter) {
    return [
      '#type' => 'operations',
      '#links' => $this->getOperations($formatter),
    ];
  }

  /**
   * Provides an array of information to build a list of operation links.
   *
   * @param \Drupal\units\UnitsFormatterInterface $formatter
   *   The formatter the operations are for.
   *
   * @return array
   *   An associative array of operation link data for this list.
   */
  public function getOperations(UnitsFormatterInterface $formatter) {
    $operations['edit'] = [
      'title' => $this->t('Edit'),
      'weight' => 10,
      'url' => Url::fromRoute('units_ui.formatter_edit_form', [
        'units_unit' => $this->unit->id(),
        'formatter' => $formatter->getUuid(),
      ]),
    ];

    $operations['delete'] = [
      'title' => $this->t('Delete'),
      'weight' => 100,
      'url' => Url::fromRoute('units_ui.formatter_delete_form', [
        'units_unit' => $this->unit->id(),
        'formatter' => $formatter->getUuid(),
      ]),
    ];

    return $operations;
  }

}

<?php

namespace Drupal\units_ui\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\units\UnitInterface;
use Drupal\units\UnitsFormatterManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the units formatter creation form.
 */
class UnitsFormatterEditForm extends FormBase {

  /**
   * The unit entity.
   *
   * @var \Drupal\units\UnitInterface
   */
  protected $unit;

  /**
   * The units formatter object.
   *
   * @var \Drupal\units\UnitsFormatterInterface
   */
  protected $formatter;

  /**
   * The units formatter plugin manager.
   *
   * @var \Drupal\units\UnitsFormatterManagerInterface
   */
  protected $formatterManager;

  /**
   * Constructs a new UnitsFormatterEditForm.
   *
   * @param \Drupal\units\UnitsFormatterManagerInterface $formatter_manager
   *   The units formatter plugin manager.
   */
  public function __construct(UnitsFormatterManagerInterface $formatter_manager) {
    $this->formatterManager = $formatter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.units.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'units_formatter_edit_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\units\UnitInterface $units_unit
   *   The unit object.
   * @param string $formatter
   *   The unique identifier of the formatter.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, UnitInterface $units_unit = NULL, $formatter = NULL) {
    $this->unit = $units_unit;
    $this->formatter = $this->prepareFormatter($formatter);

    $form['settings'] = $this->formatter->buildConfigurationForm([], $form_state);
    $form['settings']['#tree'] = TRUE;

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    $form['actions']['delete'] = [
      '#type' => 'link',
      '#title' => $this->t('Delete'),
      '#url' => Url::fromRoute('units_ui.formatter_delete_form', [
        'units_unit' => $this->unit->id(),
        'formatter' => $this->formatter->getUuid(),
      ]),
      '#attributes' => [
        'class' => ['button', 'button--danger'],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $settings = (new FormState())->setValues($form_state->getValue('settings'));
    $this->formatter->validateConfigurationForm($form, $settings);
    $form_state->setValue('settings', $settings->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();

    $settings = (new FormState())->setValues($form_state->getValue('settings'));
    $this->formatter->submitConfigurationForm($form, $settings);
    $form_state->setValue('settings', $settings->getValues());

    $this->unit->save();

    $form_state->setRedirectUrl($this->getRedirectUrl());
  }

  /**
   * Gets the URL where the user should be redirected after saving.
   *
   * @return \Drupal\Core\Url
   *   The redirect URL.
   */
  protected function getRedirectUrl() {
    return Url::fromRoute('units_ui.formatter_overview_form', [
      'units_unit' => $this->unit->id(),
    ]);
  }

  /**
   * Gets the units formatter object from the given identifier.
   *
   * @param string $formatter
   *   The unique identifier for the units formatter.
   *
   * @return \Drupal\units\UnitsFormatterInterface
   *   The units formatter object.
   */
  protected function prepareFormatter($formatter) {
    return $this->unit->getFormatter($formatter);
  }

}

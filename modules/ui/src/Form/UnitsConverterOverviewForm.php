<?php

namespace Drupal\units_ui\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\units\UnitInterface;
use Drupal\units\UnitsConverterInterface;

/**
 * Provides the units converter overview form.
 */
class UnitsConverterOverviewForm extends FormBase {

  /**
   * The unit entity.
   *
   * @var \Drupal\units\UnitInterface
   */
  protected $unit;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'units_converter_overview_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\units\UnitInterface $units_unit
   *   The unit entity.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, UnitInterface $units_unit = NULL) {
    $this->unit = $units_unit;

    $form['converters'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#empty' => $this->t('There are no converters yet.'),
    ];

    foreach ($this->unit->getConverters() as $converter) {
      $form['converters'][$converter->getUuid()] = $this->buildRow($converter);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Builds the header row for the converter listing.
   *
   * @return array
   *   A render array structure of header strings.
   */
  public function buildHeader() {
    $row['name'] = $this->t('Name');
    $row['summary'] = $this->t('Summary');
    $row['operations'] = $this->t('Operations');
    return $row;
  }

  /**
   * Builds a row for a single entry in the converter listing.
   *
   * @param \Drupal\units\UnitsConverterInterface $converter
   *   The converter for this row of the list.
   *
   * @return array
   *   A render array structure of row information.
   */
  public function buildRow(UnitsConverterInterface $converter) {
    $row['name']['#markup'] = $converter->getLabel();
    $row['summary'] = $converter->getSummary();
    $row['operations'] = $this->buildOperations($converter);
    return $row;
  }

  /**
   * Builds a renderable list of operation links for the converter.
   *
   * @param \Drupal\units\UnitsConverterInterface $converter
   *   The converter on which the linked operations will be performed.
   *
   * @return array
   *   A renderable array of operation links.
   */
  public function buildOperations(UnitsConverterInterface $converter) {
    return [
      '#type' => 'operations',
      '#links' => $this->getOperations($converter),
    ];
  }

  /**
   * Provides an array of information to build a list of operation links.
   *
   * @param \Drupal\units\UnitsConverterInterface $converter
   *   The converter the operations are for.
   *
   * @return array
   *   An associative array of operation link data for this list.
   */
  public function getOperations(UnitsConverterInterface $converter) {
    $operations['edit'] = [
      'title' => $this->t('Edit'),
      'weight' => 10,
      'url' => Url::fromRoute('units_ui.converter_edit_form', [
        'units_unit' => $this->unit->id(),
        'converter' => $converter->getUuid(),
      ]),
    ];

    $operations['delete'] = [
      'title' => $this->t('Delete'),
      'weight' => 100,
      'url' => Url::fromRoute('units_ui.converter_delete_form', [
        'units_unit' => $this->unit->id(),
        'converter' => $converter->getUuid(),
      ]),
    ];

    return $operations;
  }

}

<?php

namespace Drupal\units_ui\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\units\UnitInterface;
use Drupal\units\UnitsConverterManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the units converter creation form.
 */
class UnitsConverterEditForm extends FormBase {

  /**
   * The unit entity.
   *
   * @var \Drupal\units\UnitInterface
   */
  protected $unit;

  /**
   * The units converter object.
   *
   * @var \Drupal\units\UnitsConverterInterface
   */
  protected $converter;

  /**
   * The units converter plugin manager.
   *
   * @var \Drupal\units\UnitsConverterManagerInterface
   */
  protected $converterManager;

  /**
   * Constructs a new UnitsConverterEditForm.
   *
   * @param \Drupal\units\UnitsConverterManagerInterface $converter_manager
   *   The units converter plugin manager.
   */
  public function __construct(UnitsConverterManagerInterface $converter_manager) {
    $this->converterManager = $converter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.units.converter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'units_converter_edit_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\units\UnitInterface $units_unit
   *   The unit object.
   * @param string $converter
   *   The unique identifier of the converter.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, UnitInterface $units_unit = NULL, $converter = NULL) {
    $this->unit = $units_unit;
    $this->converter = $this->prepareConverter($converter);

    $form['settings'] = $this->converter->buildConfigurationForm([], $form_state);
    $form['settings']['#tree'] = TRUE;

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    $form['actions']['delete'] = [
      '#type' => 'link',
      '#title' => $this->t('Delete'),
      '#url' => Url::fromRoute('units_ui.converter_delete_form', [
        'units_unit' => $this->unit->id(),
        'converter' => $this->converter->getUuid(),
      ]),
      '#attributes' => [
        'class' => ['button', 'button--danger'],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $settings = (new FormState())->setValues($form_state->getValue('settings'));
    $this->converter->validateConfigurationForm($form, $settings);
    $form_state->setValue('settings', $settings->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();

    $settings = (new FormState())->setValues($form_state->getValue('settings'));
    $this->converter->submitConfigurationForm($form, $settings);
    $form_state->setValue('settings', $settings->getValues());

    $this->unit->save();

    $form_state->setRedirectUrl($this->getRedirectUrl());
  }

  /**
   * Gets the URL where the user should be redirected after saving.
   *
   * @return \Drupal\Core\Url
   *   The redirect URL.
   */
  protected function getRedirectUrl() {
    return Url::fromRoute('units_ui.converter_overview_form', [
      'units_unit' => $this->unit->id(),
    ]);
  }

  /**
   * Gets the units converter object from the given identifier.
   *
   * @param string $converter
   *   The unique identifier for the units converter.
   *
   * @return \Drupal\units\UnitsConverterInterface
   *   The units converter object.
   */
  protected function prepareConverter($converter) {
    return $this->unit->getConverter($converter);
  }

}

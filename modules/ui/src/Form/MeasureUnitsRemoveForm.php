<?php

namespace Drupal\units_ui\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\units\MeasureInterface;
use Drupal\units\UnitInterface;

/**
 * Provides the units measure unit removal form.
 */
class MeasureUnitsRemoveForm extends ConfirmFormBase {

  /**
   * The measure from which to remove the unit.
   *
   * @var \Drupal\units\MeasureInterface
   */
  protected $measure;

  /**
   * The unit to be removed.
   *
   * @var \Drupal\units\UnitInterface
   */
  protected $unit;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'units_measure_units_remove_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\units\MeasureInterface $units_measure
   *   The measure entity.
   * @param \Drupal\units\UnitInterface $units_unit
   *   The unit entity.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, MeasureInterface $units_measure = NULL, UnitInterface $units_unit = NULL) {
    $this->measure = $units_measure;
    $this->unit = $units_unit;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to remove the %unit unit from the %measure measure?', ['%unit' => $this->unit->label(), '%measure' => $this->measure->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Remove');
  }

  /**
   * Gets the URL where the user should be redirected after submission.
   *
   * @return \Drupal\Core\Url
   *   The redirect URL.
   */
  public function getRedirectUrl() {
    return Url::fromRoute('units_ui.measure_units_overview_form', [
      'units_measure' => $this->measure->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('units_ui.measure_units_overview_form', [
      'units_measure' => $this->measure->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->measure->removeUnit($this->unit);
    $this->measure->save();

    $form_state->setRedirectUrl($this->getRedirectUrl());
  }

}

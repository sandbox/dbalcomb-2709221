<?php

namespace Drupal\units_ui\Form;

/**
 * Provides the edit form for measure configuration entities.
 */
class MeasureEditForm extends MeasureFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    return $this->entity->toUrl('edit-form');
  }

}

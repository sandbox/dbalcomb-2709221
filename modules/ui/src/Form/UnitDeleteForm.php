<?php

namespace Drupal\units_ui\Form;

use Drupal\Core\Entity\EntityDeleteForm;

/**
 * Provides the delete form for unit configuration entities.
 */
class UnitDeleteForm extends EntityDeleteForm {

}

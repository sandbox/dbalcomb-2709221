<?php

namespace Drupal\units_ui\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\units\MeasureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the units measure unit add form.
 */
class MeasureUnitsAddForm extends FormBase {

  /**
   * The measure entity.
   *
   * @var \Drupal\units\MeasureInterface
   */
  protected $measure;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new MeasureUnitsAddForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'units_measure_units_add_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\units\MeasureInterface $units_measure
   *   The measure entity.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, MeasureInterface $units_measure = NULL) {
    $this->measure = $units_measure;

    $form['unit'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Find an existing unit'),
      '#target_type' => 'units_unit',
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('unit'))) {
      $form_state->setError($form['unit'], $this->t('Please select a unit.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $unit = $form_state->getValue('unit');
    $unit_entity = $this->entityTypeManager->getStorage('units_unit')->load($unit);

    $this->measure->addUnit($unit_entity);
    $this->measure->save();

    $form_state->setRedirectUrl($this->getRedirectUrl());
  }

  /**
   * Gets the URL where the user should be redirected after saving.
   *
   * @return \Drupal\Core\Url
   *   The redirect URL.
   */
  protected function getRedirectUrl() {
    return Url::fromRoute('units_ui.measure_units_overview_form', [
      'units_measure' => $this->measure->id(),
    ]);
  }

}

<?php

namespace Drupal\units_ui\Form;

use Drupal\Core\Entity\EntityDeleteForm;

/**
 * Provides the delete form for format configuration entities.
 */
class FormatDeleteForm extends EntityDeleteForm {

}

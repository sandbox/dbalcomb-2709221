<?php

namespace Drupal\units_ui\Form;

/**
 * Provides the add form for format configuration entities.
 */
class FormatAddForm extends FormatFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    return $this->entity->toUrl('edit-form');
  }

}

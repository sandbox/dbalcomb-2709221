<?php

namespace Drupal\units_ui\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\units\UnitInterface;
use Drupal\units\UnitsFormatterManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the units formatter select form.
 */
class UnitsFormatterSelectForm extends FormBase {

  /**
   * The unit entity.
   *
   * @var \Drupal\units\UnitInterface
   */
  protected $unit;

  /**
   * The units formatter plugin manager.
   *
   * @var \Drupal\units\UnitsFormatterManagerInterface
   */
  protected $formatterManager;

  /**
   * Constructs a new UnitsFormatterSelectForm.
   *
   * @param \Drupal\units\UnitsFormatterManagerInterface $formatter_manager
   *   The unit formatter plugin manager.
   */
  public function __construct(UnitsFormatterManagerInterface $formatter_manager) {
    $this->formatterManager = $formatter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.units.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'units_formatter_select_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\units\UnitInterface $units_unit
   *   The unit entity.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, UnitInterface $units_unit = NULL) {
    $this->unit = $units_unit;

    $form['formatter'] = [
      '#type' => 'select',
      '#title' => $this->t('Add a new formatter'),
      '#empty_option' => $this->t('- Select a unit formatter -'),
      '#options' => [],
    ];

    foreach ($this->formatterManager->getDefinitions() as $formatter) {
      $form['formatter']['#options'][$formatter['id']] = $formatter['label'];
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Continue'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('formatter'))) {
      $form_state->setError($form['formatter'], $this->t('Please select a valid formatter.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirectUrl($this->getRedirectUrl($form_state->getValue('formatter')));
  }

  /**
   * Gets the URL where the user should be redirected after saving.
   *
   * @param string $formatter
   *   The formatter plugin identifier.
   *
   * @return \Drupal\Core\Url
   *   The redirect URL.
   */
  protected function getRedirectUrl($formatter) {
    return Url::fromRoute('units_ui.formatter_add_form', [
      'units_unit' => $this->unit->id(),
      'formatter' => $formatter,
    ]);
  }

}

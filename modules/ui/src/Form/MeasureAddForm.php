<?php

namespace Drupal\units_ui\Form;

/**
 * Provides the add form for measure configuration entities.
 */
class MeasureAddForm extends MeasureFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    return $this->entity->toUrl('edit-form');
  }

}

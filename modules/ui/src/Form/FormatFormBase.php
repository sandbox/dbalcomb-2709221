<?php

namespace Drupal\units_ui\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the base form for format configuration entities.
 */
abstract class FormatFormBase extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t('The human-readable name.'),
      '#required' => TRUE,
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#description' => $this->t('A unique machine-readable name. Can only contain lowercase letters, numbers, and underscores.'),
      '#disabled' => !$this->entity->isNew(),
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'replace_pattern' => '[^a-z0-9_]+',
      ],
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('The administrative description.'),
      '#default_value' => $this->entity->getDescription(),
    ];

    return $form;
  }

  /**
   * Determines if the machine name is available.
   *
   * @param string $machine_name
   *   The machine name.
   * @param array $element
   *   The form element.
   *
   * @return bool
   *   TRUE if the machine name is taken, FALSE otherwise.
   */
  public function exists($machine_name, array $element) {
    $entity_storage = $this->entityTypeManager->getStorage('units_format');
    $entity = $entity_storage->load($element['#field_prefix'] . $machine_name);
    return !empty($entity);
  }

  /**
   * Gets the URL where the user should be redirected after saving.
   *
   * @return \Drupal\Core\Url
   *   The redirect URL.
   */
  protected function getRedirectUrl() {
    return $this->entity->toUrl('collection');
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $this->entity->save();
    $form_state->setRedirectUrl($this->getRedirectUrl());
  }

}

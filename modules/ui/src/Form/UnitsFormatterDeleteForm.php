<?php

namespace Drupal\units_ui\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\units\UnitInterface;

/**
 * Provides the units formatter delete form.
 */
class UnitsFormatterDeleteForm extends ConfirmFormBase {

  /**
   * The unit containing the formatter to be deleted.
   *
   * @var \Drupal\units\UnitInterface
   */
  protected $unit;

  /**
   * The units formatter to be deleted.
   *
   * @var \Drupal\units\UnitsFormatterInterface
   */
  protected $formatter;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'units_formatter_delete_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\units\UnitInterface $units_unit
   *   The unit object.
   * @param string $formatter
   *   The unique identifier of the formatter.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, UnitInterface $units_unit = NULL, $formatter = NULL) {
    $this->unit = $units_unit;
    $this->formatter = $units_unit->getFormatter($formatter);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the %formatter formatter from the %unit unit?', ['%formatter' => $this->formatter->getLabel(), '%unit' => $this->unit->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * Gets the URL where the user should be redirected after submission.
   *
   * @return \Drupal\Core\Url
   *   The redirect URL.
   */
  public function getRedirectUrl() {
    return Url::fromRoute('units_ui.formatter_overview_form', [
      'units_unit' => $this->unit->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('units_ui.formatter_overview_form', [
      'units_unit' => $this->unit->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->unit->deleteFormatter($this->formatter);

    $form_state->setRedirectUrl($this->getRedirectUrl());
  }

}

<?php

namespace Drupal\units_ui\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\units\UnitInterface;

/**
 * Provides the units converter delete form.
 */
class UnitsConverterDeleteForm extends ConfirmFormBase {

  /**
   * The unit containing the converter to be deleted.
   *
   * @var \Drupal\units\UnitInterface
   */
  protected $unit;

  /**
   * The units converter to be deleted.
   *
   * @var \Drupal\units\UnitsConverterInterface
   */
  protected $converter;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'units_converter_delete_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\units\UnitInterface $units_unit
   *   The unit object.
   * @param string $converter
   *   The unique identifier of the converter.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, UnitInterface $units_unit = NULL, $converter = NULL) {
    $this->unit = $units_unit;
    $this->converter = $units_unit->getConverter($converter);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the %converter converter from the %unit unit?', ['%converter' => $this->converter->getLabel(), '%unit' => $this->unit->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * Gets the URL where the user should be redirected after submission.
   *
   * @return \Drupal\Core\Url
   *   The redirect URL.
   */
  public function getRedirectUrl() {
    return Url::fromRoute('units_ui.converter_overview_form', [
      'units_unit' => $this->unit->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('units_ui.converter_overview_form', [
      'units_unit' => $this->unit->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->unit->deleteConverter($this->converter);

    $form_state->setRedirectUrl($this->getRedirectUrl());
  }

}

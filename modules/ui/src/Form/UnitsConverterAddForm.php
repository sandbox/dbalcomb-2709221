<?php

namespace Drupal\units_ui\Form;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\units\UnitInterface;
use Drupal\units\UnitsConverterManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the units converter creation form.
 */
class UnitsConverterAddForm extends FormBase {

  /**
   * The unit entity.
   *
   * @var \Drupal\units\UnitInterface
   */
  protected $unit;

  /**
   * The units converter object.
   *
   * @var \Drupal\units\UnitsConverterInterface
   */
  protected $converter;

  /**
   * The units converter plugin manager.
   *
   * @var \Drupal\units\UnitsConverterManagerInterface
   */
  protected $converterManager;

  /**
   * The UUID generator.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidGenerator;

  /**
   * Constructs a new UnitsConverterAddForm.
   *
   * @param \Drupal\units\UnitsConverterManagerInterface $converter_manager
   *   The units converter plugin manager.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_generator
   *   The UUID generator.
   */
  public function __construct(UnitsConverterManagerInterface $converter_manager, UuidInterface $uuid_generator) {
    $this->converterManager = $converter_manager;
    $this->uuidGenerator = $uuid_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.units.converter'),
      $container->get('uuid')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'units_converter_add_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\units\UnitInterface $units_unit
   *   The unit object.
   * @param string $converter
   *   The units converter plugin identifier.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, UnitInterface $units_unit = NULL, $converter = NULL) {
    $this->unit = $units_unit;
    $this->converter = $this->prepareConverter($converter);

    $form['settings'] = $this->converter->buildConfigurationForm([], $form_state);
    $form['settings']['#tree'] = TRUE;

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('units_ui.converter_overview_form', [
        'units_unit' => $this->unit->id(),
      ]),
      '#attributes' => ['class' => ['button']],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $settings = (new FormState())->setValues($form_state->getValue('settings'));
    $this->converter->validateConfigurationForm($form, $settings);
    $form_state->setValue('settings', $settings->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();

    $settings = (new FormState())->setValues($form_state->getValue('settings'));
    $this->converter->submitConfigurationForm($form, $settings);
    $form_state->setValue('settings', $settings->getValues());

    $this->unit->addConverter($this->converter->getConfiguration());
    $this->unit->save();

    $form_state->setRedirectUrl($this->getRedirectUrl());
  }

  /**
   * Gets the URL where the user should be redirected after saving.
   *
   * @return \Drupal\Core\Url
   *   The redirect URL.
   */
  protected function getRedirectUrl() {
    return Url::fromRoute('units_ui.converter_overview_form', [
      'units_unit' => $this->unit->id(),
    ]);
  }

  /**
   * Gets the units converter object from the given plugin identifier.
   *
   * @param string $converter
   *   The units converter plugin identifier.
   *
   * @return \Drupal\units\UnitsConverterInterface
   *   The units converter object.
   */
  protected function prepareConverter($converter) {
    $converter = $this->converterManager->createInstance($converter, [
      'uuid' => $this->uuidGenerator->generate(),
    ]);
    $converter->setWeight(count($this->unit->getConverters()));
    return $converter;
  }

}

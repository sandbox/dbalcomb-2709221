<?php

namespace Drupal\units_ui\Form;

/**
 * Provides the add form for unit configuration entities.
 */
class UnitAddForm extends UnitFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    return $this->entity->toUrl('edit-form');
  }

}

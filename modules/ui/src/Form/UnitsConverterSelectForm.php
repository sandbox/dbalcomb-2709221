<?php

namespace Drupal\units_ui\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\units\UnitInterface;
use Drupal\units\UnitsConverterManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the units converter select form.
 */
class UnitsConverterSelectForm extends FormBase {

  /**
   * The unit entity.
   *
   * @var \Drupal\units\UnitInterface
   */
  protected $unit;

  /**
   * The units converter plugin manager.
   *
   * @var \Drupal\units\UnitsConverterManagerInterface
   */
  protected $converterManager;

  /**
   * Constructs a new UnitsConverterSelectForm.
   *
   * @param \Drupal\units\UnitsConverterManagerInterface $converter_manager
   *   The unit converter plugin manager.
   */
  public function __construct(UnitsConverterManagerInterface $converter_manager) {
    $this->converterManager = $converter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.units.converter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'units_converter_select_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\units\UnitInterface $units_unit
   *   The unit entity.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, UnitInterface $units_unit = NULL) {
    $this->unit = $units_unit;

    $form['converter'] = [
      '#type' => 'select',
      '#title' => $this->t('Add a new converter'),
      '#empty_option' => $this->t('- Select a unit converter -'),
      '#options' => [],
    ];

    foreach ($this->converterManager->getDefinitions() as $converter) {
      $form['converter']['#options'][$converter['id']] = $converter['label'];
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Continue'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('converter'))) {
      $form_state->setError($form['converter'], $this->t('Please select a valid converter.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirectUrl($this->getRedirectUrl($form_state->getValue('converter')));
  }

  /**
   * Gets the URL where the user should be redirected after saving.
   *
   * @param string $converter
   *   The converter plugin identifier.
   *
   * @return \Drupal\Core\Url
   *   The redirect URL.
   */
  protected function getRedirectUrl($converter) {
    return Url::fromRoute('units_ui.converter_add_form', [
      'units_unit' => $this->unit->id(),
      'converter' => $converter,
    ]);
  }

}

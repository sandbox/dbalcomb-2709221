<?php

namespace Drupal\units_ui\Form;

use Drupal\Core\Entity\EntityDeleteForm;

/**
 * Provides the delete form for measure configuration entities.
 */
class MeasureDeleteForm extends EntityDeleteForm {

}

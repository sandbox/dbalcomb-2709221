<?php

namespace Drupal\units_ui;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Provides a listing of unit configuration entities.
 */
class UnitListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Name');
    $header['description'] = $this->t('Description');
    $header['measures'] = $this->t('Measures');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['description'] = $entity->getDescription();

    $entity_type_manager = \Drupal::service('entity_type.manager');
    $measure_storage = $entity_type_manager->getStorage('units_measure');
    $measures = [];

    foreach ($measure_storage->loadMultiple() as $measure) {
      if (in_array($entity, $measure->getUnits())) {
        $link = '<a href="' . $measure->toUrl('edit-form')->toString() . '">';
        $link .= $measure->label();
        $link .= '</a>';
        $measures[] = $link;
      }
    }

    $row['measures']['data']['#markup'] = implode(', ', $measures);

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#empty'] = $this->t('There are no units yet.');
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    $operations['formatters'] = [
      'title' => $this->t('Manage formatters'),
      'url' => Url::fromRoute('units_ui.formatter_overview_form', [
        'units_unit' => $entity->id(),
      ]),
      'weight' => 20,
    ];

    $operations['converters'] = [
      'title' => $this->t('Manage converters'),
      'url' => Url::fromRoute('units_ui.converter_overview_form', [
        'units_unit' => $entity->id(),
      ]),
      'weight' => 30,
    ];

    return $operations;
  }

}

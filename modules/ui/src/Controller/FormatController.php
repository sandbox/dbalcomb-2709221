<?php

namespace Drupal\units_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\units\FormatInterface;

/**
 * Defines a controller for format configuration entities.
 */
class FormatController extends ControllerBase {

  /**
   * Provides the page title for the format edit form.
   *
   * @param \Drupal\units\FormatInterface $units_format
   *   The format entity.
   *
   * @return string
   *   The page title.
   */
  public function editFormTitle(FormatInterface $units_format) {
    return $this->t('Edit %label format', ['%label' => $units_format->label()]);
  }

}

<?php

namespace Drupal\units_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\units\UnitInterface;

/**
 * Defines a controller for unit configuration entities.
 */
class UnitController extends ControllerBase {

  /**
   * Provides the page title for the unit edit form.
   *
   * @param \Drupal\units\UnitInterface $units_unit
   *   The unit entity.
   *
   * @return string
   *   The page title.
   */
  public function editFormTitle(UnitInterface $units_unit) {
    return $this->t('Edit %label unit', ['%label' => $units_unit->label()]);
  }

}

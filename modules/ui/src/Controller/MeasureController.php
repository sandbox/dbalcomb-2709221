<?php

namespace Drupal\units_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\units\MeasureInterface;

/**
 * Defines a controller for measure configuration entities.
 */
class MeasureController extends ControllerBase {

  /**
   * Provides the page title for the measure edit form.
   *
   * @param \Drupal\units\MeasureInterface $units_measure
   *   The measure entity.
   *
   * @return string
   *   The page title.
   */
  public function editFormTitle(MeasureInterface $units_measure) {
    return $this->t('Edit %label measure', ['%label' => $units_measure->label()]);
  }

}

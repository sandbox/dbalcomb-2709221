# Units of Measurement

## Summary

The Units module introduces the ability to create units of measurement and make
conversions between them.

## Configuration

Enable the Units UI submodule to provide users with the ability to create,
update and administer custom units of measurement using the configuration pages
found at Admin >> Configuration >> Measurement.

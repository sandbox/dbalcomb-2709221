<?php

namespace Drupal\units;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a format entity.
 */
interface FormatInterface extends ConfigEntityInterface {

  /**
   * Gets the description.
   *
   * @return string
   *   The description of the format.
   */
  public function getDescription();

}

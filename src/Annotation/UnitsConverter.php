<?php

namespace Drupal\units\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the units converter annotation.
 *
 * @Annotation
 */
class UnitsConverter extends Plugin {

  /**
   * The machine-readable name of the converter.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the converter.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The administrative description of the converter.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}

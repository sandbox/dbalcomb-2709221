<?php

namespace Drupal\units\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the units formatter annotation.
 *
 * @Annotation
 */
class UnitsFormatter extends Plugin {

  /**
   * The machine-readable name of the formatter.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the formatter.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The administrative description of the formatter.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}

<?php

namespace Drupal\units;

use Drupal\Core\Plugin\DefaultLazyPluginCollection;

/**
 * Provides a collection for units formatters.
 */
class UnitsFormatterCollection extends DefaultLazyPluginCollection {

  /**
   * {@inheritdoc}
   */
  protected $pluginKey = 'plugin';

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\units\UnitsFormatterInterface
   *   The units formatter object.
   */
  public function &get($instance_id) {
    return parent::get($instance_id);
  }

  /**
   * {@inheritdoc}
   */
  public function sortHelper($aID, $bID) {
    $a_weight = $this->get($aID)->getWeight();
    $b_weight = $this->get($bID)->getWeight();
    if ($a_weight == $b_weight) {
      return 0;
    }

    return ($a_weight < $b_weight) ? -1 : 1;
  }

}

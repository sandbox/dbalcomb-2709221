<?php

namespace Drupal\units;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a measure entity.
 */
interface MeasureInterface extends ConfigEntityInterface {

  /**
   * Gets the description.
   *
   * @return string
   *   The description of the measure.
   */
  public function getDescription();

  /**
   * Gets the units that belong to the measure.
   *
   * @return \Drupal\units\UnitInterface[]
   *   The array of units, keyed by the unit identifier.
   */
  public function getUnits();

  /**
   * Adds a unit to the measure.
   *
   * @param \Drupal\units\UnitInterface $unit
   *   A unit entity to add.
   */
  public function addUnit(UnitInterface $unit);

  /**
   * Removes a unit from the measure.
   *
   * @param \Drupal\units\UnitInterface $unit
   *   A unit entity to remove.
   */
  public function removeUnit(UnitInterface $unit);

}

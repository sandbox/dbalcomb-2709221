<?php

namespace Drupal\units\Plugin\Units\Converter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\units\UnitInterface;
use Drupal\units\UnitsConverterBase;

/**
 * Defines the 'linear' units converter plugin.
 *
 * @UnitsConverter(
 *   id = "linear",
 *   label = @Translation("Linear"),
 *   description = @Translation("Allows units to be converted by applying a factor.")
 * )
 */
class Linear extends UnitsConverterBase {

  /**
   * Gets the factor.
   *
   * @return float
   *   The factor.
   */
  public function getFactor() {
    return $this->configuration['factor'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $summary = parent::getSummary();
    $summary['#markup'] = $this->t('Factor: @factor', ['@factor' => $this->getFactor()]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'factor' => 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, UnitInterface $unit) {
    $factor = $this->configuration['factor'];

    if (!is_numeric($factor)) {
      throw new \Exception('Invalid factor.');
    }

    foreach ($unit->getConverters() as $converter) {
      if ($converter->getPluginId() == 'linear') {
        $target = $converter->getFactor();

        if (!is_numeric($target)) {
          continue;
        }

        return $value * $factor / $target;
      }
    }

    throw new \Exception('Unable to convert to the specified unit.');
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['factor'] = [
      '#type' => 'number',
      '#step' => 'any',
      '#title' => $this->t('Factor'),
      '#description' => $this->t('The scale factor used to convert between units.'),
      '#default_value' => $this->getFactor(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['factor'] = $form_state->getValue('factor');
  }

}

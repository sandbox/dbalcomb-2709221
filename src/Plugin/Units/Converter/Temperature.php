<?php

namespace Drupal\units\Plugin\Units\Converter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\units\UnitInterface;
use Drupal\units\UnitsConverterBase;

/**
 * Defines the 'temperature' units converter plugin.
 *
 * @UnitsConverter(
 *   id = "temperature",
 *   label = @Translation("Temperature"),
 *   description = @Translation("Allows temperature units to be converted.")
 * )
 */
class Temperature extends UnitsConverterBase {

  /**
   * Gets the list of sources allowed in configuration.
   *
   * @return array
   *   An associative array of source names mapped to labels.
   */
  protected function getSources() {
    return [
      'celsius' => $this->t('Celsius'),
      'kelvin' => $this->t('Kelvin'),
      'fahrenheit' => $this->t('Fahrenheit'),
    ];
  }

  /**
   * Gets the source temperature.
   *
   * @return string
   *   The source temperature.
   */
  public function getSource() {
    return $this->configuration['source'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $summary = parent::getSummary();
    $sources = $this->getSources();
    $source = $this->getSource();
    $summary['#markup'] = $this->t('Source: @source', ['@source' => $sources[$source]]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'source' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, UnitInterface $unit) {
    $source = $this->configuration['source'];
    $target = NULL;

    if (empty($source)) {
      throw new \Exception('Unspecified source temperature.');
    }

    if (!in_array($source, array_keys($this->getSources()))) {
      throw new \Exception('Invalid source temperature.');
    }

    foreach ($unit->getConverters() as $converter) {
      if ($converter->getPluginId() == 'temperature') {
        $target = $converter->getSource();

        if (empty($target)) {
          continue;
        }

        if (!in_array($target, array_keys($this->getSources()))) {
          continue;
        }

        switch ($source . ':' . $target) {
          case 'celsius:fahrenheit':
            return $value * (9 / 5) + 32;

          case 'celsius:kelvin':
            return $value + 273.15;

          case 'fahrenheit:kelvin':
            return ($value - 32) * (5 / 9) + 273.15;

          case 'fahrenheit:celsius':
            return ($value - 32) * (5 / 9);

          case 'kelvin:fahrenheit':
            return ($value - 273.15) * (9 / 5) + 32;

          case 'kelvin:celsius':
            return $value - 273.15;
        }
      }
    }

    throw new \Exception('Unable to convert to the specified unit.');
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['source'] = [
      '#type' => 'select',
      '#title' => $this->t('Source temperature'),
      '#description' => $this->t('The quantity that this unit represents.'),
      '#default_value' => $this->configuration['source'],
      '#empty_option' => '- None -',
      '#empty_value' => '',
      '#options' => $this->getSources(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['source'] = $form_state->getValue('source');
  }

}

<?php

namespace Drupal\units\Plugin\Units\Formatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\units\UnitInterface;
use Drupal\units\UnitsFormatterBase;

/**
 * Defines the 'number' units formatter plugin.
 *
 * @UnitsFormatter(
 *   id = "number",
 *   label = @Translation("Number"),
 *   description = @Translation("Formats the unit as a number.")
 * )
 */
class NumberFormatter extends UnitsFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'scale' => 2,
      'decimal_separator' => '.',
      'thousand_separator' => ',',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary(UnitInterface $unit) {
    $summary = parent::getSummary($unit);
    $summary['#markup'] = $this->format(1234.1234567890, $unit);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function format($value, UnitInterface $unit, $options = []) {
    $options = array_merge($this->configuration, $options);
    return number_format($value, $options['scale'], $options['decimal_separator'], $options['thousand_separator']);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['scale'] = [
      '#type' => 'number',
      '#title' => $this->t('Decimal places'),
      '#description' => $this->t('The number of digits to the right of the decimal.'),
      '#default_value' => $this->configuration['scale'],
      '#min' => 0,
      '#max' => 10,
    ];

    $form['decimal_separator'] = [
      '#type' => 'select',
      '#title' => $this->t('Decimal marker'),
      '#description' => $this->t('The separator to use for decimals.'),
      '#default_value' => $this->configuration['decimal_separator'],
      '#options' => [
        '.' => t('Decimal point'),
        ',' => t('Comma'),
      ],
    ];

    $form['thousand_separator'] = [
      '#type' => 'select',
      '#title' => $this->t('Thousand marker'),
      '#description' => $this->t('The separator to use for thousands.'),
      '#default_value' => $this->configuration['thousand_separator'],
      '#options' => [
        '' => $this->t('- None -'),
        '.' => $this->t('Decimal point'),
        ',' => $this->t('Comma'),
        ' ' => $this->t('Space'),
        chr(8201) => $this->t('Thin space'),
        "'" => $this->t('Apostrophe'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['scale'] = $form_state->getValue('scale');
    $this->configuration['decimal_separator'] = $form_state->getValue('decimal_separator');
    $this->configuration['thousand_separator'] = $form_state->getValue('thousand_separator');
  }

}

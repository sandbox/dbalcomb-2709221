<?php

namespace Drupal\units\Plugin\Units\Formatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\units\UnitInterface;

/**
 * Defines the 'symbol' units formatter plugin.
 *
 * @UnitsFormatter(
 *   id = "symbol",
 *   label = @Translation("Symbol"),
 *   description = @Translation("Formats the unit as a number with symbol.")
 * )
 */
class SymbolFormatter extends NumberFormatter {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'symbol' => '',
      'symbol_position' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary(UnitInterface $unit) {
    $summary = parent::getSummary($unit);
    $summary['#markup'] = $this->format(1234.1234567890, $unit);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function format($value, UnitInterface $unit, $options = []) {
    $number = parent::format($value, $unit, $options);
    $symbol = $this->configuration['symbol'];

    switch ($this->configuration['symbol_position']) {
      case 'left':
        return $symbol . ' ' . $number;

      case 'right':
        return $number . ' ' . $symbol;
    }

    return $number;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['symbol'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Symbol'),
      '#description' => $this->t('The symbol for the unit.'),
      '#required' => TRUE,
      '#maxlength' => 100,
      '#default_value' => $this->configuration['symbol'],
    ];

    $form['symbol_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Symbol position'),
      '#description' => $this->t('The position of the symbol.'),
      '#default_value' => $this->configuration['symbol_position'],
      '#options' => [
        '' => 'Hidden',
        'left' => 'Left',
        'right' => 'Right',
      ],
    ];

    return $form + parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['symbol'] = $form_state->getValue('symbol');
    $this->configuration['symbol_position'] = $form_state->getValue('symbol_position');
  }

}

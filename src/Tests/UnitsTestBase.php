<?php

namespace Drupal\units\Tests;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\simpletest\WebTestBase;

/**
 * Defines the base class for units module tests.
 */
abstract class UnitsTestBase extends WebTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['units'];

  /**
   * A test user to run the tests with.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The permissions to give to the test user.
   *
   * @var array
   */
  protected $permissions = [
    'administer units measures',
    'administer units units',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser($this->permissions);
    $this->drupalLogin($this->user);
  }

  /**
   * Creates a new entity.
   *
   * @param string $entity_type
   *   The entity type to be created.
   * @param array $values
   *   An array of values to pass to the entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   A new entity.
   */
  protected function createEntity($entity_type, array $values) {
    $entity_type_manager = \Drupal::service('entity_type.manager');
    $entity_storage = $entity_type_manager->getStorage($entity_type);
    $entity = $entity_storage->create($values);
    $status = $entity->save();
    $message = new FormattableMarkup('Created %type entity %id.', [
      '%type' => $entity->getEntityType()->id(),
      '%id' => $entity->id(),
    ]);
    $this->assertEqual($status, SAVED_NEW, $message);
    $entity = $entity_storage->load($entity->id());
    return $entity;
  }

}

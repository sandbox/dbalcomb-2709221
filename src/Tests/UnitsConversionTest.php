<?php

namespace Drupal\units\Tests;

/**
 * Tests the units module unit conversion.
 *
 * @group units
 */
class UnitsConversionTest extends UnitsTestBase {

  /**
   * Tests unit conversion.
   */
  public function testUnitConversion() {
    $unit_from = $this->createEntity('units_unit', [
      'id' => $this->randomMachineName(),
      'label' => 'Unit one',
    ]);
    $unit_from->save();

    $unit_to = $this->createEntity('units_unit', [
      'id' => $this->randomMachineName(),
      'label' => 'Unit two',
    ]);
    $unit_to->save();

    $measure = $this->createEntity('units_measure', [
      'id' => $this->randomMachineName(),
      'label' => 'Test measure',
      'units' => [
        $unit_from->id(),
        $unit_to->id(),
      ],
    ]);
    $measure->save();

    $quantity = rand(1, 1000);
    $factor_from = rand(1, 1000);
    $factor_to = rand(1, 1000);

    $unit_from->addConverter([
      'plugin' => 'linear',
      'settings' => [
        'factor' => $factor_from,
      ],
    ]);
    $unit_from->save();

    $unit_to->addConverter([
      'plugin' => 'linear',
      'settings' => [
        'factor' => $factor_to,
      ],
    ]);
    $unit_to->save();

    $this->assertEqual($quantity, units_convert($quantity, $unit_from->id(), $unit_from->id()), 'Converting within the same unit yields the same amount.');
    $this->assertEqual($quantity * $factor_from / $factor_to, units_convert($quantity, $unit_from->id(), $unit_to->id()), 'Converting an arbitrary amount happens according to the logic of conversion.');
  }

}

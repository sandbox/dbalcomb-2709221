<?php

namespace Drupal\units;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Defines an interface for the discovery of units formatters.
 */
interface UnitsFormatterManagerInterface extends PluginManagerInterface {

}

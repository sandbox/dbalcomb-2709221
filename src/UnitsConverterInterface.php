<?php

namespace Drupal\units;

use Drupal\Component\Plugin\ConfigurablePluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines an interface for units converters.
 */
interface UnitsConverterInterface extends ConfigurablePluginInterface, PluginFormInterface, PluginInspectionInterface {

  /**
   * Gets the unique identifier of the converter.
   *
   * @return string
   *   The unique identifier of the converter.
   */
  public function getUuid();

  /**
   * Gets the label of the converter.
   *
   * @return string
   *   The label of the converter.
   */
  public function getLabel();

  /**
   * Gets the weight of the converter.
   *
   * @return int
   *   The weight of the converter.
   */
  public function getWeight();

  /**
   * Sets the weight of the converter.
   *
   * @param int $weight
   *   The new weight of the converter.
   *
   * @return $this
   */
  public function setWeight($weight);

  /**
   * Gets a render array summarizing the configuration of the converter.
   *
   * @return array
   *   A render array.
   */
  public function getSummary();

  /**
   * Converts values between units of measurement.
   *
   * @param float $value
   *   The value to be converted.
   * @param \Drupal\units\UnitInterface $unit
   *   The unit to which the value should be converted.
   *
   * @return float
   *   The converted value.
   *
   * @throws \Exception
   *   Thrown when the conversion has failed.
   */
  public function convert($value, UnitInterface $unit);

}

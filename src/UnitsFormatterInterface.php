<?php

namespace Drupal\units;

use Drupal\Component\Plugin\ConfigurablePluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines an interface for units formatters.
 */
interface UnitsFormatterInterface extends ConfigurablePluginInterface, PluginFormInterface, PluginInspectionInterface {

  /**
   * Gets the unique identifier of the formatter.
   *
   * @return string
   *   The unique identifier of the formatter.
   */
  public function getUuid();

  /**
   * Gets the label of the formatter.
   *
   * @return string
   *   The label of the formatter.
   */
  public function getLabel();

  /**
   * Gets the weight of the formatter.
   *
   * @return int
   *   The weight of the formatter.
   */
  public function getWeight();

  /**
   * Sets the weight of the formatter.
   *
   * @param int $weight
   *   The new weight of the formatter.
   *
   * @return $this
   */
  public function setWeight($weight);

  /**
   * Gets the identifier of the format that the formatter is connected to.
   *
   * @return string
   *   The identifier of the format.
   */
  public function getFormatId();

  /**
   * Gets a render array summarizing the configuration of the formatter.
   *
   * @param \Drupal\units\UnitInterface $unit
   *   The unit to use for the summary.
   *
   * @return array
   *   A render array.
   */
  public function getSummary(UnitInterface $unit);

  /**
   * Formats a value for a given unit of measurement.
   *
   * @param float $value
   *   The value to be formatted.
   * @param \Drupal\units\UnitInterface $unit
   *   The unit for which the value should be formatted.
   * @param array $options
   *   Additional formatting options.
   *
   * @return string
   *   The formatted value.
   *
   * @throws \Exception
   *   Thrown when the formatting has failed.
   */
  public function format($value, UnitInterface $unit, $options = []);

}

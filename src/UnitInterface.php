<?php

namespace Drupal\units;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a unit entity.
 */
interface UnitInterface extends ConfigEntityInterface {

  /**
   * Gets the description.
   *
   * @return string
   *   The description of the unit.
   */
  public function getDescription();

  /**
   * Gets the specified formatter.
   *
   * @param string $uuid
   *   The unique identifier of the formatter.
   *
   * @return \Drupal\units\UnitsFormatterInterface
   *   The units formatter object.
   */
  public function getFormatter($uuid);

  /**
   * Gets the formatters for the unit.
   *
   * @return \Drupal\units\UnitsFormatterCollection|\Drupal\units\UnitsFormatterInterface[]
   *   The units formatter plugin collection.
   */
  public function getFormatters();

  /**
   * Adds a new formatter to the unit.
   *
   * @param array $configuration
   *   An array of formatter configuration.
   *
   * @return string
   *   The unique identifier of the formatter.
   */
  public function addFormatter(array $configuration);

  /**
   * Deletes a formatter from the unit.
   *
   * @param \Drupal\units\UnitsFormatterInterface $formatter
   *   The units formatter object.
   *
   * @return $this
   */
  public function deleteFormatter(UnitsFormatterInterface $formatter);

  /**
   * Gets the specified converter.
   *
   * @param string $uuid
   *   The unique identifier of the converter.
   *
   * @return \Drupal\units\UnitsConverterInterface
   *   The units converter object.
   */
  public function getConverter($uuid);

  /**
   * Gets the converters for the unit.
   *
   * @return \Drupal\units\UnitsConverterCollection|\Drupal\units\UnitsConverterInterface[]
   *   The units converter plugin collection.
   */
  public function getConverters();

  /**
   * Adds a new converter to the unit.
   *
   * @param array $configuration
   *   An array of converter configuration.
   *
   * @return string
   *   The unique identifier of the converter.
   */
  public function addConverter(array $configuration);

  /**
   * Deletes a converter from the unit.
   *
   * @param \Drupal\units\UnitsConverterInterface $converter
   *   The units converter object.
   *
   * @return $this
   */
  public function deleteConverter(UnitsConverterInterface $converter);

}

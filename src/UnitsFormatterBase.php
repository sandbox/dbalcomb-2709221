<?php

namespace Drupal\units;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;

/**
 * Provides a base units formatter.
 */
abstract class UnitsFormatterBase extends PluginBase implements UnitsFormatterInterface {

  /**
   * The unique identifier of the formatter.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The weight of the formatter.
   *
   * @var int
   */
  protected $weight;

  /**
   * The identifier of the format that the formatter is connected to.
   *
   * @var string
   */
  protected $format;

  /**
   * {@inheritdoc}
   */
  public function __construct($configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function getUuid() {
    return $this->uuid;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->weight = $weight;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormatId() {
    return $this->format;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary(UnitInterface $unit) {
    return [
      '#markup' => '',
      '#definition' => $this->getPluginDefinition(),
      '#configuration' => $this->getConfiguration(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return [
      'uuid' => $this->uuid,
      'weight' => $this->weight,
      'format' => $this->format,
      'plugin' => $this->pluginId,
      'provider' => $this->pluginDefinition['provider'],
      'settings' => $this->configuration,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $configuration += [
      'uuid' => '',
      'weight' => 0,
      'format' => '',
      'settings' => [],
    ];
    $this->uuid = $configuration['uuid'];
    $this->weight = $configuration['weight'];
    $this->format = $configuration['format'];
    $this->configuration = $configuration['settings'] + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies['module'] = [$this->pluginDefinition['provider']];

    if (!empty($this->format)) {
      $dependencies['config'] = ['units.format.' . $this->format];
    }

    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

}

<?php

namespace Drupal\units\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\units\MeasureInterface;
use Drupal\units\UnitInterface;

/**
 * Defines the measure configuration entity.
 *
 * @ConfigEntityType(
 *   id = "units_measure",
 *   label = @Translation("Measure"),
 *   admin_permission = "administer units measures",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_prefix = "measure",
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "units"
 *   }
 * )
 */
class Measure extends ConfigEntityBase implements MeasureInterface {

  /**
   * The machine-readable name of the measure.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the measure.
   *
   * @var string
   */
  protected $label;

  /**
   * The administrative description of the measure.
   *
   * @var string
   */
  protected $description;

  /**
   * The units that belong to the measure.
   *
   * @var array
   */
  protected $units = [];

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getUnits() {
    return $this->entityTypeManager()->getStorage('units_unit')->loadMultiple($this->units);
  }

  /**
   * {@inheritdoc}
   */
  public function addUnit(UnitInterface $unit) {
    if (!in_array($unit->id(), $this->units)) {
      $this->units[] = $unit->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function removeUnit(UnitInterface $unit) {
    if (($key = array_search($unit->id(), $this->units)) !== FALSE) {
      unset($this->units[$key]);
    }
  }

}

<?php

namespace Drupal\units\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\units\UnitInterface;
use Drupal\units\UnitsConverterCollection;
use Drupal\units\UnitsConverterInterface;
use Drupal\units\UnitsFormatterCollection;
use Drupal\units\UnitsFormatterInterface;

/**
 * Defines the unit configuration entity.
 *
 * @ConfigEntityType(
 *   id = "units_unit",
 *   label = @Translation("Unit"),
 *   admin_permission = "administer units units",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_prefix = "unit",
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "formatters",
 *     "converters"
 *   }
 * )
 */
class Unit extends ConfigEntityBase implements UnitInterface, EntityWithPluginCollectionInterface {

  /**
   * The machine-readable name of the unit.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the unit.
   *
   * @var string
   */
  protected $label;

  /**
   * The administrative description of the unit.
   *
   * @var string
   */
  protected $description;

  /**
   * The array of formatters for the unit.
   *
   * @var array
   */
  protected $formatters = [];

  /**
   * Holds the collection of formatters that are used by the unit.
   *
   * @var \Drupal\units\UnitsFormatterCollection
   */
  protected $formattersCollection;

  /**
   * The array of converters for the unit.
   *
   * @var array
   */
  protected $converters = [];

  /**
   * Holds the collection of converters that are used by the unit.
   *
   * @var \Drupal\units\UnitsConverterCollection
   */
  protected $convertersCollection;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormatter($uuid) {
    return $this->getFormatters()->get($uuid);
  }

  /**
   * Returns the units formatter plugin manager.
   *
   * @return \Drupal\units\UnitsFormatterManagerInterface
   *   The units formatter plugin manager.
   */
  protected function getFormatterManager() {
    return \Drupal::service('plugin.manager.units.formatter');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormatters() {
    if (!$this->formattersCollection) {
      $this->formattersCollection = new UnitsFormatterCollection(
        $this->getFormatterManager(),
        $this->formatters
      );
      $this->formattersCollection->sort();
    }
    return $this->formattersCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function addFormatter(array $configuration) {
    $configuration['uuid'] = $this->uuidGenerator()->generate();
    $this->getFormatters()->addInstanceId($configuration['uuid'], $configuration);
    return $configuration['uuid'];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteFormatter(UnitsFormatterInterface $formatter) {
    $this->getFormatters()->removeInstanceId($formatter->getUuid());
    $this->save();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConverter($uuid) {
    return $this->getConverters()->get($uuid);
  }

  /**
   * Returns the units converter plugin manager.
   *
   * @return \Drupal\units\UnitsConverterManagerInterface
   *   The units converter plugin manager.
   */
  protected function getConverterManager() {
    return \Drupal::service('plugin.manager.units.converter');
  }

  /**
   * {@inheritdoc}
   */
  public function getConverters() {
    if (!$this->convertersCollection) {
      $this->convertersCollection = new UnitsConverterCollection(
        $this->getConverterManager(),
        $this->converters
      );
      $this->convertersCollection->sort();
    }
    return $this->convertersCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function addConverter(array $configuration) {
    $configuration['uuid'] = $this->uuidGenerator()->generate();
    $this->getConverters()->addInstanceId($configuration['uuid'], $configuration);
    return $configuration['uuid'];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteConverter(UnitsConverterInterface $converter) {
    $this->getConverters()->removeInstanceId($converter->getUuid());
    $this->save();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'formatters' => $this->getFormatters(),
      'converters' => $this->getConverters(),
    ];
  }

}

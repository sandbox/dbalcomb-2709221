<?php

namespace Drupal\units\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\units\FormatInterface;

/**
 * Defines the format configuration entity.
 *
 * @ConfigEntityType(
 *   id = "units_format",
 *   label = @Translation("Format"),
 *   admin_permission = "administer units formats",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_prefix = "format",
 *   config_export = {
 *     "id",
 *     "label",
 *     "description"
 *   }
 * )
 */
class Format extends ConfigEntityBase implements FormatInterface {

  /**
   * The machine-readable name of the format.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the format.
   *
   * @var string
   */
  protected $label;

  /**
   * The administrative description of the format.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

}

<?php

namespace Drupal\units;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages units converters.
 */
class UnitsConverterManager extends DefaultPluginManager implements UnitsConverterManagerInterface {

  /**
   * Constructs a new UnitsConverterManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Units/Converter', $namespaces, $module_handler, 'Drupal\units\UnitsConverterInterface', 'Drupal\units\Annotation\UnitsConverter');
    $this->alterInfo('units_converter_info');
    $this->setCacheBackend($cache_backend, 'units_converters');
  }

}

<?php

namespace Drupal\units;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages units formatters.
 */
class UnitsFormatterManager extends DefaultPluginManager implements UnitsFormatterManagerInterface {

  /**
   * Constructs a new UnitsFormatterManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Units/Formatter', $namespaces, $module_handler, 'Drupal\units\UnitsFormatterInterface', 'Drupal\units\Annotation\UnitsFormatter');
    $this->alterInfo('units_formatter_info');
    $this->setCacheBackend($cache_backend, 'units_formatters');
  }

}
